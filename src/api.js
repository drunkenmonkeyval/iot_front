import axios from 'axios'

const api = 'https://iot-palvelin-mity.oulu.fi/iot_test/public/api/iot'

const login = async credentials => {
    const response = await axios.post('https://iot-palvelin-mity.oulu.fi/iot_test/public/api/login_check', credentials)
    return response.data
}

const getDevices = async () => {
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }
    const response = await axios.get(`${api}/devices`, config)
    return response.data
}

const getLocations = async () => {
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))

    if (!storageToken) {
        return 
    }
    
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.get(`${api}/locations`, config)
    return response.data
}

const addDevice = async (device) => {
    console.log(device)
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.post(`${api}/devices`, device, config)
    return response.data
}

const getDeviceData = async (id) => {
    const response = await axios.get(`${api}/data/${id}`)
    return response.data
}

const getSensorData = async user => {
    console.log(user.token)
    const response = await axios.get(`${api}/sensor/data`, {
        headers: { Authorization: `Bearer ${user.token}` }
    })
    return response.data
}

const getDeviceSensorData = async (id) => {
    console.log(id)
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }
    const response = await axios.get(`${api}/sensor/${id}`, config)
    return response.data
}

const getDeviceElsysData = async (id) => {
    console.log(id)
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }
    const response = await axios.get(`${api}/elsys/${id}`, config)
    return response.data
}

const getDeviceMilesData = async (id) => {
    console.log(id)
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }
    const response = await axios.get(`${api}/miles/${id}`, config)
    return response.data
}



const getLocationDevices = async (id) => {
    console.log(id)
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.get(`${api}/location/${id}`, config)
    return response.data
}

const setLimits = async (req) => {
    console.log(req)
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.post(`${api}/limit`, req, config)
    return response.data
}

const getLimits = async id  => {

    console.log(id)

    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.post(`${api}/limit/${id}`, config)
    return response.data

}

const getAllSensorData = async () => {
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))

    console.log(storageToken)

    if (!storageToken) {
        return 
    }

    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }
    const response = await axios.get(`${api}/sensor/`, config)
    return response.data
}

const getAllSensorRelatedData = async () => {
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))

    console.log(storageToken)

    if (!storageToken) {
        return 
    }

    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }
    const response = await axios.get(`${api}/sensors/`, config)
    return response.data
}

const getAllEnvRelatedData = async () => {
    
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))

    console.log(storageToken)

    if (!storageToken) {
        return 
    }

  /*   const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.get(`${api}/sensors/`, config)

    return response.data */
}


const addWeather = async (wdata) => {
    const storageToken = JSON.parse(window.localStorage.getItem("loggedUser"))
    const config = {
        headers: { Authorization: `Bearer ${storageToken.token}` }
    }

    const response = await axios.post(`${api}/weather/data`, wdata, config)
    return response.data
}

export default { getDevices, 
                 getLocations, 
                 getLocationDevices,
                 getSensorData,
                 addDevice,
                 getDeviceData,
                 getDeviceSensorData,
                 login,
                 setLimits,
                 getLimits,
                 getAllSensorData,
                 getAllSensorRelatedData,
                 getAllEnvRelatedData,
                 getDeviceElsysData,
                 getDeviceMilesData,
                 addWeather
                }