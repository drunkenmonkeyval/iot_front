import React, { useState, useEffect } from "react";
import api from "./api";
import SideBar from "./components/sidebar.js";
import Login from "./components/login.js";
import Admin from "./components/adminform.js";
import ChangeDevice from "./components/changedevice.js";
import AddDevice from "./components/add_device";
import Locations from "./components/locations.js";
import Device from "./components/device";
import Labs from "./components/labs";
import Weather from "./components/weather";


import BoxTest from "./components/box";

import "./App.css";
import {
    Button,
    useToast,
    Flex,
    Box,
    Heading,
    Image,
    Spacer,
    Text
} from "@chakra-ui/react";

const App = () => {

    const [user, setUser] = useState(null)
    const [devices, setDevices] = useState([]);
    const [localDevices, setLocalDevices] = useState([]);
    const [locations, setLocations] = useState([]);
    const [localLocs, setLocalLocs] = useState([]);

    //ALL DATA
    const [allData, setAllData] = useState([]);

    const resetState = () => {
        setDevices(null)
        setLocations(null)
        setAllData(null)
    }

    const logout = () => {
        const loggedUser = window.localStorage.getItem("loggedUser")
        if (loggedUser) {
            window.localStorage.removeItem("loggedUser")
            setUser(null)
            resetState()
        }
    }

    useEffect(() => {
        const loggedUser = window.localStorage.getItem("loggedUser")
        if (loggedUser) {
            const user = JSON.parse(loggedUser)
            setUser(user)
        }
    }, [])

    useEffect(() => {
        if (user) {
            api
                .getDevices()
                .then((devices) => {
                    setLocalDevices(devices);
                })
                .catch((err) => {
                    console.log(err.response.status)

                    if (err.response.status === 500) {
                        infoToast(0)
                    }
                    else {
                        infoToast()
                        resetState()
                    }
                })
        }
    }, [])

    useEffect(() => {
        if (user) {
            api
                .getLocations()
                .then((locations) => {
                    setLocalLocs(locations);
                })
                .catch((err) => {
                    console.log(err.response.status)

                    if (err.response.status === 500) {
                        infoToast(0)
                    }
                    else {
                        infoToast()
                        resetState()
                    }
                })
        }
    }, [])

    const toast = useToast();
    const infoToast = (status) => {
        if (status === 0) {
            toast({
                title: "Virhe",
                description: "Tarkasta palvelin",
                status: "error",
                duration: 1500,
                isClosable: true,
            })
        }
        else {
            toast({
                title: "Virhe",
                description: "Ei asiaa tänne",
                status: "error",
                duration: 1500,
                isClosable: true,
            })
        }
    }

    return (
        
        <>  
        

      {/*       <Weather />  */}

            {user ?
                <Flex>
                    <Box p='2'>
                        <SideBar devices={devices}
                            setDevices={setDevices}
                            setLocations={setLocations}
                            user={user}
                            setUser={setUser}
                            allData={allData}
                            setAllData={setAllData} />
                    </Box>
                    <Spacer />
                    <Box p='2' bg="gray.50" /* rounded='md' */ boxShadow='outline' mt="5px" width="100%">
                {/*         <Image src="https://i.media.fi/incoming/ntft79/7157714.jpg/alternates/FREE_720/7157714.jpg" alt="tytti" width="50%"/> */}
                        <Text textAlign={['left', 'center']} >
                            <Heading size='lg'>Iot</Heading>
                        </Text>
                    </Box>
                    <Spacer />
                    <Box p='2'>
                        <Button colorScheme='blue'
                            variant='solid'
                            onClick={logout}>
                            Kirjaudu ulos
                        </Button>
                    </Box>
                </Flex>

                : <Login user={user} setUser={setUser} />
            }

            {user && allData && allData.length ? <Labs allData={allData}/> : '' }
            
            {/* <BoxTest /> */}

             {/* <Admin />  */}

             {/* <AddDevice user={user} /> 

             

          

            {/*    {user ?
                <SideBar devices={devices}
                    setDevices={setDevices}
                    setLocations={setLocations}
                    user={user}
                    setUser={setUser} />
                : <Login user={user} setUser={setUser} />
            }

            {user ? <Button colorScheme='blue'
                variant='solid'
                onClick={logout}>
                Kirjaudu ulos
            </Button>

                : ''
            } */}

            {user && locations ? <Locations locations={locations} setLocations={setLocations}
                localLocs={localLocs} setLocalLocs={setLocalLocs}
                user={user} /> : ''}
            {user && devices && devices.length ? <Device devices={devices} setDevices={setDevices}
                localDevices={localDevices} setLocalDevices={setLocalDevices}
                user={user} /> : ''}
        </>
    )
}

export default App;
