import React, { useState } from "react";
import api from "../api";
import {
    Avatar,
    Button,
    Box,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Radio,
    RadioGroup,
    Stack,
    useDisclosure,
    useToast,
} from "@chakra-ui/react";


export default function SideBar({ /* devices,  */setDevices, /* setLocation, */ setLocations, user, setUser, allData, setAllData}) {

    const bleImg = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Bluetooth.svg/1200px-Bluetooth.svg.png'
    const labImg = 'https://www.oulu.fi/sites/default/files/styles/full_width/public/collection/header/IMG_6526-Edit.jpg?itok=zkkccL6c'

    const { isOpen, onOpen, onClose } = useDisclosure();
    const [placement, setPlacement] = React.useState("left");
    // const [devices, setDevices] = useState([]);

    

    console.log(user)

    const getSensorData = async () => {
        onClose()
        const data = 
            await api.getAllSensorRelatedData()
                  .catch(err => {
                      console.log(err)
                      //setUser(null)
                  })
        
        console.log(data)
        setAllData(data)
        if (data === undefined) { 
            setUser(null)
            infoToast()  
        }  
    }

    console.log(allData)


    const toast = useToast();
    const infoToast = (status) => {
        if (status === 0) {
            toast({
                title: "Virhe",
                description: "Tarkasta palvelin",
                status: "error",
                duration: 1500,
                isClosable: true,
            });
        } else {

            toast({
                title: "Virhe",
                description: "Ei asiaa tänne",
                status: "error",
                duration: 1500,
                isClosable: true,
            });

            /*  toast({
                 title: "Data ladattu",
                 description: "Data ladattu",
                 status: "success",
                 duration: 1500,
                 isClosable: true,
             }); */
        }
    };

    const getDeviceData = () => {
        onClose()
        api
            .getDevices()
            .then((devices) => {
                setDevices(devices);
                setLocations(null)
                console.log(devices);

                //infoToast()
            })
            .catch((err) => {
                console.log(err.response.status)

                if (err.response.status === 500) {
                    infoToast(0)
                }
                else {
                    infoToast()
                    setDevices(null)
                    setLocations(null)
                    setUser(null)
                    //setLocalDevices(null)
                }
            })
    }

    const getLocations = () => {
        onClose()
        api
            .getLocations()
            .then((locations) => {
                setLocations(locations);
                setDevices(null)
                console.log(locations);
                if (locations === undefined) { 
                    setUser(null)
                    infoToast()  
                }    
            })
            .catch((err) => {
                console.log(err.response.status)

                if (err.response.status === 500) {
                    infoToast(0)
                }
                else {
                    infoToast()
                    setDevices(null)
                    setLocations(null)
                    setUser(null)
                    //setLocalDevices(null)
                }
            })
    }

    const addDevice = () => {
        api.addDevice({
            name: "ENIGMAAAAAAAAAA",
            mac: "AAAA!#22",
            type: 2
        })
            .then((devs) => {

                console.log(devs);
            })
            .catch(err => console.log(err.response.data))
    }

    const getDeviceDataa = (id) => {
        //console.log(id)
        api.getDeviceData(id)
            .then((devs) => {

                console.log(devs);
            })
            .catch(err => console.log(err.response.data))
    }

    return (
        <>
          {/*  <Box p='2'> */}
            <Button colorScheme="green" onClick={onOpen}>
                Mittaukset
            </Button>
          {/* </Box>  */}
            <Drawer placement={placement} onClose={onClose} isOpen={isOpen}>
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerHeader borderBottomWidth="1px">Mittaukset</DrawerHeader>
                    <DrawerBody>

                   {/*  BLE   */}
                     {/*    <p>
                            <Avatar
                                size={'xl'}
                                src={bleImg}
                                alt={'Laitteet'}
                                mb={4}
                                pos={'relative'}
                                onClick={getDeviceData}
                            />
                            BLE-LAITTEET
                        </p>  */}                      
                        
                      {/*    <p>
                            <Avatar
                                size={'xl'}
                                src={labImg}
                                alt={'Tilat'}
                                mb={4}
                                pos={'relative'}                
                                onClick={getLocations}
                            />
                            TILAT
                        </p> */}

                        <p>
                            <Avatar
                                size={'xl'}
                                src={labImg}
                                alt={'Tilat'}
                                mb={4}
                                pos={'relative'}                
                                onClick={getSensorData}
                            />
                            OLOSUHDEMITTAUKSET
                        </p>


                      {/*   <p><Button colorScheme="green" onClick={() => getDeviceDataa(15)}>
                            Hae
                        </Button>
                        </p>

 */}

                    </DrawerBody>
                    <DrawerFooter>
                        <Button colorScheme="blue" mr={200} onClick={onClose}>
                            Sulje
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}
