import React from "react"
import * as FileSaver from 'file-saver'
import * as XLSX from 'xlsx'
import { Box, Button} from '@chakra-ui/react'

export default function CSV ({ data }) {

    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';
    let elsysData = data
/* 
     elsysData = elsysData.map((d, index) => {

                //console.log(new Date(Number(d.time) * 1000).toLocaleDateString("en-US"))
                //d.time = Number(new Date((Number(d.time)) * 1000).toLocaleDateString("en-US"))
                //d.time = new Date(Number(d.time) * 1000).toLocaleDateString()

                d.time = d.time * 12

                return d

              
            }
    )  */

    const generateCsv = () => {
        const workSheet = XLSX.utils.json_to_sheet(elsysData)
        const workBook = { Sheets: { 'data': workSheet }, SheetNames: ['data'] }
        const excelBuffer = XLSX.write(workBook, { bookType: 'xlsx', type: 'array' })
        const data = new Blob([excelBuffer], { type: fileType })
        FileSaver.saveAs(data, "Data" + fileExtension)
    }

    return (
        <Box p='2'>
        <Button colorScheme='teal'
            variant='solid'
            onClick={generateCsv}>
            Vie Exceliin
        </Button>
        </Box>
    )
}