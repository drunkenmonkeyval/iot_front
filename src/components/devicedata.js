import React, { useState, useEffect } from "react";
import api from "../api";
import Graph from "../components/plot.js";
import { MdArrowBack, MdSensors } from "react-icons/md";
import {
    Heading,
    Avatar,
    Button,
    Box,
    Center,
    Text,
    Stack,
    HStack,
    VStack,
    Link,
    Badge,
    useColorModeValue,
} from '@chakra-ui/react';
import Locations from "./locations";

export default function DeviceData({ devData }) {

    console.log(devData)

    return (
        <>
            <VStack spacing="8px">
                <Center py={6}>
                    <Box
                        /*     maxW={'225px'} */
                        /*     w={'full'} */
                        bg={useColorModeValue('white', 'gray.900')}
                        boxShadow={'2xl'}
                        rounded={'lg'}
                        mr={2}
                        p={6}
                        textAlign={'center'}

                    /*  onClick={() => getData()} */

                    >
                        <Heading mb={4}> DATA </Heading>


                        {devData && devData.length ?

                            devData.map(data =>
                                <Text fontWeight={600} color={'gray.500'} mb={4}>
                                    {data.id} {data.mac}
                                </Text>
                             )
                            : ''
                        }
                        {/*    {
                        devData.map(data =>
                              <Text fontWeight={600} color={'gray.500'} mb={4}>
                          { data.name }
                        </Text>
                        )
                      }   */}

                        <Text
                            textAlign={'center'}
                            color={useColorModeValue('gray.700', 'gray.400')}
                            px={3}>

                        </Text>
                    </Box>

                     <Graph devData={devData} /> 
                     
                </Center>

            </VStack>
        </>
    )
}