import api from "../api";
import { Form, Field, Formik } from 'formik';
import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    FormErrorMessage,
    Input,
    Checkbox,
    Stack,
    Select,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue,
    useToast
} from "@chakra-ui/react";

export default function AddDevice({ user }) {

    const toast = useToast();
    const infoToast = (status) => {
        //if (status === 0) {
        toast({
            title: "Virhe",
            description: "Laite on jo olemassa",
            status: "error",
            duration: 1500,
            isClosable: true,
        });
        // }
    }

    const handleSelect = value => {

        console.log('kljsdiod')
        let error
        if (!value) {
            error = 'Syötä alaraja'
        }
        return error
    }


    const validateName = value => {
        let error
        if (!value) {
            error = 'Syötä nimi'
        }
        return error
    }

    const validatePassword = value => {
        let error
        if (!value) {
            error = 'Syötä tyyppi'
        }
        return error
    }

    const validateType = value => {
        let error
        if (!value) {
            error = 'Syötä tyyppi'
        }
        return error
    }

    return (
        <Formik
            initialValues={{ Name: '', Mac: '', Type: '' }}
            onSubmit={async (values, actions) => {

                console.log(values)

                try {
                    const user = await api.addDevice(values)
                    //window.localStorage.setItem('loggedUser', JSON.stringify(user)

                    //setUser(user)
                    actions.setSubmitting(false)
                }
                catch (e) {
                    e.response.status === 422 ? infoToast() : console.log(e.response.status)
                    //infoToast()
                    actions.setSubmitting(false)
                }
            }
            }
        >
            {(props) => (
                <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
                    <Stack align={"center"}>
                        <Heading fontSize={"4xl"}>Lisää laite</Heading>
                    </Stack>
                    <Box
                        rounded={"lg"}
                        boxShadow={"lg"}
                        p={8}
                    >
                        <Form>
                            <Field name='Name' validate={validateName}>
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.Name && form.touched.Name}
                                        autoComplete="Name">
                                        <FormLabel htmlFor='Name'>Nimi</FormLabel>
                                        <Input {...field} id='Name' placeholder='Name' autocomplete="Name" />
                                        <FormErrorMessage>{form.errors.Name}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>


                            <Field name='Mac' validate={validateName}>
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.Mac && form.touched.Mac}
                                        autoComplete="Mac">
                                        <FormLabel htmlFor='Mac'>Mac</FormLabel>
                                        <Input {...field} id='Mac' placeholder='Mac' autocomplete="Mac" />
                                        <FormErrorMessage>{form.errors.Mac}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>

                          {/*     <Field name='Type' validate={validateType} >
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.Type && form.touched.Type} >
                                        <FormLabel htmlFor='tyyppi'>Tyyppi</FormLabel>

                                        <Input {...field} id='Type' placeholder='Type'
                                            autoComplete="tyyppi" />
                                            
                                        <FormErrorMessage>{form.errors.Type}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>  */} 

                            

                            <FormLabel htmlFor='Type'>Tyyppi</FormLabel>
                            <Field
                                component="select"
                                id="Type"
                                name="Type"
                            >
                                <option value="1">Olosuhde</option>
                                <option value="2">Ymp</option>
                            </Field>

                            
                            <Button
                                mt={4}
                                colorScheme='teal'
                                isLoading={props.isSubmitting}
                                type='submit'
                            >
                                Syötä
                            </Button>
                        </Form>
                    </Box>
                </Stack>
            )
            }
        </Formik>
    )
}
