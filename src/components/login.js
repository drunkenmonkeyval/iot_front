import api from "../api";
import { Form, Field, Formik } from 'formik';
import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    FormErrorMessage,
    Input,
    Checkbox,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue,
    useToast
} from "@chakra-ui/react";

export default function Login({ user, setUser }) {

    const toast = useToast();
    const infoToast = (status) => {
        if (status === 0) {
            toast({
                title: "Virhe",
                description: "Tarkasta palvelin",
                status: "error",
                duration: 1500,
                isClosable: true,
            })
        }

        else {
            toast({
            title: "Virhe",
            description: "Tarkasta käyttäjätunnus ja salasana",
            status: "error",
            duration: 1500,
            isClosable: true,
          });
        }
    }


    const validateName = value => {
        let error
        if (!value) {
            error = 'Syötä käyttäjätunnus'
        }
        return error
    }

    const validatePassword = value => {
        let error
        if (!value) {
            error = 'Syötä salasana'
        }
        return error
    }

    return (
        <Formik
            initialValues={{ username: '', password: '' }}
            onSubmit={async (values, actions) => {
                try {
                    const user = await api.login(values)
                    window.localStorage.setItem('loggedUser', JSON.stringify(user)
                    )
                    setUser(user)
                    actions.setSubmitting(false)
                }
                catch (err) {
                    if (err.response.status === 500) {
                        //console.log('dwefw')
                        infoToast(0)
                    }
                    else if (err.response.status === 401) {
                        //console.log('UI)UI')
                        infoToast(1)
                    }
                   /*  else {
                        console.log('VITU')
                        infoToast(1)
                    } */
                    //infoToast()
                    actions.setSubmitting(false)
                }
             }
            }
        >
            {(props) => (
                <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
                    <Stack align={"center"}>
                        <Heading fontSize={"4xl"}>Kirjautuminen</Heading>
                    </Stack>
                    <Box
                        rounded={"lg"}
                        boxShadow={"lg"}
                        p={8}
                    >
                        <Form>
                            <Field name='username' validate={validateName}>
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.username && form.touched.username}
                                        autoComplete="username">
                                        <FormLabel htmlFor='username'>Käyttäjätunnus</FormLabel>
                                        <Input {...field} id='username' placeholder='tunnus' autocomplete="username" />
                                        <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>

                            <Field name='password' validate={validatePassword} >
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.password && form.touched.password} >
                                        <FormLabel htmlFor='password'>Salasana</FormLabel>
                                        <Input {...field} id='password' placeholder='salasana' type="password"
                                            autoComplete="current-password" />
                                        <FormErrorMessage>{form.errors.password}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>
                            <Button
                                mt={4}
                                colorScheme='teal'
                                isLoading={props.isSubmitting}
                                type='submit'
                            >
                                Kirjaudu
                            </Button>
                        </Form>
                    </Box>
                </Stack>
            )
            }
        </Formik>
    )
}
