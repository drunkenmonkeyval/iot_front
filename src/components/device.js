import React from "react";
import Card from "../components/card.js";
import { Box, Stack, SimpleGrid, HStack, VStack, StackDivider } from '@chakra-ui/react'

//import api from "../api";

export default function Device ({ devices, 
                                 setDevices,
                                 localDevices,
                                 setLocalDevices
                                })
{
    const deviceTypes = ['Ympäristömittauslaite', 'Olosuhdesensori']
    const img = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Bluetooth.svg/1200px-Bluetooth.svg.png'
    
    return (
        <SimpleGrid minChildWidth='300px' spacing='1px'>       
            {devices.map((i, index) => {
                    console.log(index)
                    return (
                        <Card key={i.id}
                            id={i.id}
                            name={i.nimi}
                            p1={i.mac}
                            p2={deviceTypes[i.tyyppi]}
                            img={img} 
                            devices={devices} 
                            setDevices={setDevices}
                            localDevices={localDevices}
                            setLocalDevices={setLocalDevices}
                        />
                   )
                })
            }

        </SimpleGrid>
    )
}