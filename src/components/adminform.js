import React, { useState, useEffect } from "react";
import api from "../api";
import { Form, Field, Formik } from 'formik';
import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    FormErrorMessage,
    Input,
    Checkbox,
    Stack,
    Select,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue,
    useToast
} from "@chakra-ui/react";

export default function Admin({ user, setUser }) {

    const [devData, setDevData] = useState([]);


    const handleSelect = value => {

        console.log('kljsdiod')
        let error
        if (!value) {
            error = 'Syötä alaraja'
        }
        return error
    }

    const toast = useToast();
    const infoToast = (status) => {
        //if (status === 0) {
        toast({
            title: "Virhe",
            description: "Laitteella on jo asetetut rajat",
            status: "error",
            duration: 1500,
            isClosable: true,
        });
        // }
    }

    useEffect(() => {
        api
            .getDevices()
            .then((devices) => {
                setDevData(devices)
                //console.log(devices)
            })
            .catch((err) => {
                console.log(err.response.status)
            })
    }, [])


    const validateName = value => {
        let error
        if (!value) {
            error = 'Syötä alaraja'
        }
        return error
    }

    const validatePassword = value => {
        let error
        if (!value) {
            error = 'Syötä yläraja'
        }
        return error
    }

    return (
        <Formik
            initialValues={{ ala: '', yla: '', laite: '' }}
            onSubmit={async (values, actions) => {

                console.log(values)

                    try {
                       const user = await api.setLimits(values)
                       actions.setSubmitting(false)
                   }
                   catch (e) {
                    e.response.status === 422 ? infoToast() : console.log(e.response.status)
                       //infoToast()
                       actions.setSubmitting(false)
                   }
                } 
            }
            
        >
            {(props) => (
                <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
                    <Stack align={"center"}>
                        <Heading fontSize={"4xl"}>Raja-arvot</Heading>
                    </Stack>
                    <Box
                        rounded={"lg"}
                        boxShadow={"lg"}
                        p={8}
                    >
                        <Form>
                            <Field name='ala' validate={validateName}>
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.ala && form.touched.ala}
                                        autoComplete="ala">
                                        <FormLabel htmlFor='ala'>Alaraja</FormLabel>
                                        <Input {...field} id='ala' placeholder='Alaraja' autocomplete="ala" />
                                        <FormErrorMessage>{form.errors.ala}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>

                            <Field name='yla' validate={validatePassword} >
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.yla && form.touched.yla} >
                                        <FormLabel htmlFor='yla'>Yläraja</FormLabel>
                                        <Input {...field} id='yla' placeholder='yla'
                                            autoComplete="yla" />
                                        <FormErrorMessage>{form.errors.yla}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field>

                            {/*  <Field name='laite' validate={validatePassword} >
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.laite && form.touched.laite} >
                                        <FormLabel htmlFor='laite'>Laite</FormLabel>
                                        <Input {...field} id='laite' placeholder='Tyyppi'
                                            autoComplete="laite" />
                                        <FormErrorMessage>{form.errors.laite}</FormErrorMessage>
                                    </FormControl>
                                )}
                            </Field> */}

                            {/*     <Field name='laite'>
                                {({ field, form }) => (
                                    <FormControl isInvalid={form.errors.laite && form.touched.laite} >
                                        <FormLabel htmlFor='laite'>Laite</FormLabel>
                                        <Select  {...field} placeholder='Valitse laite' name="laite">
                                            {devData.map((num, index) => <option value={num} key={index} >{num.nimi}</option>)}
                                        </Select>
                                    </FormControl>
                                )}

                            </Field> */}

                        <FormLabel htmlFor='laite'>Laite</FormLabel>
                            <Field
                                component="select"
                                id="laite"
                                name="laite"
                                /* placeholder='Valitse laite'  */
                                /* multiple={true} */
                            >     
                               <option value="NY">Valitse laite</option>            
                               {devData.map((num, index) => <option value={num.id} key={index} >{num.nimi}</option>)}         
                            </Field>

                            <Button
                                mt={4}
                                colorScheme='teal'
                                isLoading={props.isSubmitting}
                                type='submit'
                            >
                                Syötä
                            </Button>
                        </Form>
                    </Box>
                </Stack>
            )
            }
        </Formik>
    )
}
