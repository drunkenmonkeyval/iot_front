import React, { useState, useEffect } from "react";
import api from "../api";
import LabData from "../components/labdata";
import Graph from "../components/plot.js";

import BoxTest from "../components/box";

import {
    Heading,
    Avatar,
    Button,
    Box,
    Center,
    Text,
    Stack,
    HStack,
    VStack,
    Link,
    Badge,
    useColorModeValue,
} from '@chakra-ui/react';

export default function Labs({ allData }) {

    const [lab, setLab] = useState([])
    const [labData, setLabData] = useState([])
    const [graphData, setGraphData] = useState([])
    const [devType, setDevType] = useState(0)

    console.log(allData)
    const setData = async (lid) => {

        console.log(lid)

        setLab(allData.filter((dev, ind) => dev.lid == lid))

        console.log(lab)

        setLabData(null)
        setGraphData(null)

        //HAE JO TÄSSÄ KAIKKI LAITTEET
        const dev = await setSomething(lid)
    }

    const setSomething = async (lid) => {

        console.log(lid)

        const data = await api.getLocationDevices(lid)
            .catch(err => {
                console.log(err)
                //setUser(null)
            })

        setLabData(data)

       const devId = data[0].id

        //HAE JO TÄSSÄ KAIKKI MITTAUDATA
       const dev = await getSensData(devId, data)

    }

    console.log(labData)

    const getSensData = async (devId, lData) => {
        
        console.log(devId)

        console.log(labData[0])

        const devType = lData && lData.length ? lData[0].type : 0

        setDevType(devType)

        const call = devType === 1 ? api.getDeviceSensorData(devId) :
                     devType === 3 ? api.getDeviceElsysData(devId) :
                     api.getDeviceMilesData(devId)


        console.log(call)

        const data = await call
            .catch(err => {
                console.log(err)
                //setUser(null)
            })

        setGraphData(data)
        console.log(data)
    }

    return (
        <>

            <HStack>

                {allData && allData.length ?

                    <BoxTest allData={allData} setData={setData} title={"Sijainti"} />
                    /*  allData.map((data, index) =>
                         <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => setData(data.lid)}>
                             {data.lid} {data.bname} {data.lname}
                         </Text>
                     ) */
                    : ''
                }

             {/*    {lab && lab.length ?
                    <BoxTest lab={lab} setSomething={setSomething} title={"Tila"} />

                    : ''
                }
             */}

                {labData && labData.length ?
                    <BoxTest labData={labData} 
                        getSensData={getSensData}
                        graphData={graphData}
                        /* title={"Laitteeet"} */
                         title={lab[0].lname}
                         devType={devType}/>

                    : ''
                }

            </HStack>

            <HStack>
               {/*  {labData && labData.length ?
                    <BoxTest labData={labData} getSensData={getSensData} title={lab[0].lname}/>

                    : ''
                }
                */}
              {/*    {graphData && graphData.length ?
                    <>
                        <BoxTest graphData={graphData} title={"Mittaukset"}/>
                  
                    </>    

                    : ''
                } */}
        </HStack>
            
        <HStack>
            {/*  {graphData && graphData.length ?
                    <>
                        <Graph graphData={graphData}/> 
                    </>    

                    : ''
            }  */}
        </HStack>
            

        

          {/*   <VStack>
                
            </VStack>
 */}
            {/* <HStack spacing="24px">
                <Center px={16} py={6}>
                    {allData && allData.length ?
                        <Box
                            boxShadow={'2xl'}
                            rounded={'lg'}
                            mr={2}
                            p={6}
                            textAlign={'center'}
                        >
                            {allData && allData.length ?
                                allData.map((data, index) =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => setData(data.lid)}>
                                        {data.lid} {data.bname} {data.lname}
                                    </Text>
                                )
                                : ''
                            }
                        </Box>
                        : ''
                    }

                </Center>
                <Center px={16} py={6}>
                    {lab && lab.length ?
                        <Box
                            boxShadow={'2xl'}
                            rounded={'lg'}
                            mr={12}
                            p={6}
                            textAlign={'center'}
                        >
                            {lab && lab.length ?
                                lab.map(data =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => setSomething(data.lid)}>
                                        {data.lid} {data.lname}
                                    </Text>
                                )
                                : ''
                            }
                        </Box>
                        : ''
                    }
                </Center>
            </HStack>

            <HStack spacing="24px">
                <Center px={16} py={6} >
                    {labData && labData.length ?
                        <Box
                            boxShadow={'2xl'}
                            rounded={'lg'}
                            mr={2}
                            p={6}
                            textAlign={'center'}
                        >
                            {labData && labData.length ?
                                labData.map(data =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => getSensData(data.id)} >
                                        {data.id}
                                    </Text>
                                )
                                : ''
                            }
                        </Box>
                        : ''
                    }
                </Center>

                <Center px={16} py={6}>
                    {graphData && graphData.length ?
                        <Box
                            boxShadow={'2xl'}
                            rounded={'lg'}
                            mr={2}
                            p={6}
                            textAlign={'center'}
                        >
                            {graphData && graphData.length ?
                                graphData.map(data =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4}>
                                        {data.id} {data.mac}
                                    </Text>
                                )
                                : ''
                            }
                        </Box>
                        : ''
                    }
                </Center>

                <Center py={6}>
                    {graphData && graphData.length ?
                        <Box
                            boxShadow={'2xl'}
                            rounded={'lg'}
                            mr={2}
                            p={6}
                            textAlign={'center'}
                        >
                            <Graph graphData={graphData} />
                        </Box>
                        : ''
                    }
                </Center>
            </HStack> */}
        </>
    )
}

