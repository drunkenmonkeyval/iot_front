import React, { useState, useEffect } from "react";
import Card from "../components/card.js";
import api from "../api"
import { Box, Stack, HStack, VStack, StackDivider } from '@chakra-ui/react'

//import api from "../api";

export default function Locations({ locations, 
                                    setLocations,
                                    localLocs,
                                    setLocalLocs
                                 })
                                  
{   

   /*  useEffect(() => {
        api.getLocations()
        .then(locs => console.log((locs)))
    }, [locations])
    */
    
    const img = 'https://www.oulu.fi/sites/default/files/styles/full_width/public/collection/header/IMG_6526-Edit.jpg?itok=zkkccL6c'
    console.log(locations)

    
    return (
        <HStack spacing="8px">
            {locations.map(i => {
                return <Card key={i.id}
                    id={i.id}
                    name={i.name}
                    p1={i.temp}
                    p2={i.humidity}
                    img={img}
                    locations={locations}
                    setLocations={setLocations}
                    localLocs={localLocs}
                    setLocalLocs={setLocalLocs}
                    building={i.building}  
                />
            })}
        </HStack>
    )
}