import React, { useState, useEffect } from "react";
import api from "../api";
import Graph from "../components/plot.js";
import DeviceData from "../components/devicedata";
import { MdArrowBack, MdSensors } from "react-icons/md";
import {
    Heading,
    Avatar,
    Button,
    Box,
    Center,
    Text,
    Stack,
    HStack,
    VStack,
    Link,
    Badge,
    useColorModeValue,
} from '@chakra-ui/react';
import Locations from "./locations";

export default function DataCard({ user, locations }) {

    const [sensorData, setSensorData] = useState([]);
    const [limits, setLimits] = useState([]);
    const [devData, setDevData] = useState([]);

    console.log(locations[0].id)

    console.log(devData)

    const getDeviceData = id => {
        console.log(id)
         api.getDeviceSensorData(id)
            .then(data => {
                setDevData(data)
                console.log(data)}
                ) 
    }

    useEffect(() => {
            api
                .getLocationDevices(locations[0].id)
                .then((data) => {
                    console.log(data)
                    setSensorData(data);
                })
                .catch((err) => {
                    console.log(err.response.status)
                })
    }, [])

    

  /*   useEffect(() => {
        if (user) {
            api
                .getSensorData(user)
                .then((data) => {
                    console.log(data)
                    setSensorData(data);
                })
                .catch((err) => {
                    console.log(err.response.status)
                })
        }
    }, [])

    const setData = () => {
        console.log()
    } */

    return (
        <>
            <VStack spacing="8px">
                <Center py={6}>
                    <Box
                        /*     maxW={'225px'} */
                        /*     w={'full'} */
                        bg={useColorModeValue('white', 'gray.900')}
                        boxShadow={'2xl'}
                        rounded={'lg'}
                        mr={2}
                        p={6}
                        textAlign={'center'}

                    /*  onClick={() => getData()} */

                    >
                        <Heading mb={4}> LAITTEET </Heading>

                        <Heading fontSize={'2xl'} fontFamily={'body'}>

                            {/* {
                                sensorData.map((data => data.name))
                            } */}

                            {/*  64466446
                      tt434t4
                      t54
                      WWEW  
                      KP
                      46564
                      64
                      64
                      34
                      1
                      1 */}
                        </Heading>

                      {
                        sensorData.map(data =>
                              <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => getDeviceData(data.id)}>
                          Nimi { data.name } Rajat {data.low} {data.high}
                        </Text>
                            )
                      } 
                        
                        <Text
                            textAlign={'center'}
                            color={useColorModeValue('gray.700', 'gray.400')}
                            px={3}>

                        </Text>
                    </Box>


                    {/* <Graph sensorData={sensorData} /> */}

                    {devData && devData.length ?
                      <DeviceData devData={devData} />
                     : ''
                    } 
                </Center>
                
            </VStack>
        </>
    )
}