import React, { useState, useEffect } from "react"
import Plot from 'react-plotly.js'
import {
    Box,
    useColorModeValue,
} from '@chakra-ui/react'
import CSV from "../components/export_csv"

export default function Graph({ graphData, gData, devType }) {

    const xData = [1, 2, 3]
    const yData = [6, 12, 18]

    console.log(graphData)

    console.log(gData.slice(gData.length - 10))

    const data = Object.entries(graphData)

    console.log(data[1][1])

    console.log('TYYPPI', devType)

    console.log(gData.length)

   /*  const [graph, setGraph] = useState([])

    setGraph(Object.values(graphData))

    console.log(graph)
 */
    return (
        <>
   {/*      <Box  borderWidth='2px' borderRadius='lg' mr={2}> */}
  {/*           }
     {/*    </Box> */}

        <Box  borderWidth='2px' borderRadius='lg' mr={2}>
  
        <Plot
            useResizeHandler
            /*  config={{ responsive: true }} */

            /*className="w-full h-full" */
            style={{ width: "100%", height: "100%" }}
            data={[
                {
                    //x: data[2].map(time => time),
                    x: gData
                       .slice(gData/* .length - 2016 */) 
                       .map(meas => new Date(devType === 1 ? meas.timestamp * 1000 : meas.time * 1000)),
                    y: gData
                        .slice(gData/* .length - 2016 */) 
                        .map(meas =>  
                            graphData.title === "Ilmankosteus" ? meas.humidity :
                            graphData.title === "TVoc" ? meas.voc :
                            graphData.title === "Hiukkaspitoisuus" ?  meas.mass_pm1 : 

                            graphData.title === "Kosteus" ? meas.humidity :

                            
                            graphData.title === "Lämpötila" ?  meas.temp : 
                            graphData.title === "Hiilidioksidi" ?  meas.co2 : 
                            graphData.title === "Valoisuus" ?  meas.light :
                            graphData.title === "Liike" ?  meas.motion : 

                            graphData.title === "Paine" ?  meas.pressure :
                            graphData.title === "PM_2_5" ?  meas.pm_2_5 :
                            graphData.title === "TVOC" ? meas.tvoc :
                            graphData.title === "PM_10" ?  meas.pm_10 :
                            graphData.title === "Ozone" ?  meas.ozone :
                            graphData.title === "KOSTEUS" ? meas.hum

                            : '' 
                        ),
                    
                  /* x: data[2].map(time => new Date(time * 1000)),
                    y: data[1].map(hum => hum), */
                       
                    /* .toLocaleDateString("fi-FI") */
                    
                    /*  x: sensorData.map(data => data.sleep),
                        y: sensorData.map(data => data.sleep), */
                    /*  x: graphData.map(data => data.id),
                        y: graphData.map(data => data.humidity), */

                    type: 'scatter',
                    mode: 'lines+markers',
                    marker: { color: 'red' },
                },
            ]}
            layout={{ xaxis: { title: 'Aika' }, yaxis: { title: graphData.title }, autosize: true }}
        />
        </Box>
        </>
         )
}

