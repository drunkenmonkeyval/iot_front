import React, { useEffect } from "react"
import api from "../api";
import axios from 'axios'

export default function Weather() {

    const url = 'https://www.ilmatieteenlaitos.fi/observation-data?station=126736'

    const getWeatherData = async () => {
        try {
            const response = await axios.get(url)
            const data = response.data
            const temp = data.t2m[data.t2m.length - 1]
            const wind = data.WindSpeedMS[data.WindSpeedMS.length - 1]
            const windDir = data.WindDirection[data.WindDirection.length - 1]
            const humidity = data.Humidity[data.Humidity.length - 1]
            const time = data.latestObservationTime

            const weatherData = {
                temp: temp[1],
                humidity: humidity[1],
                windspeed: wind[1],
                winddir: windDir[1],
                time: time
            }

            addWeatherData(weatherData)

        } catch (err) {
            console.log(err.response)
        }
    }

    const addWeatherData = async (weatherData) => {
        if (Object.values(weatherData)[0] !== '') {
            const data = await api.addWeather(weatherData)
                .catch(err => {
                    console.log(err)
                })
        }
    }

    useEffect(() => {
        const weatherInterval = setInterval(getWeatherData, 600000)
        return () => {
            clearInterval(weatherInterval)
        }
    }, [])

    return (
        <div>

        </div>
    )
}
