import React, { useState, useEffect } from "react";
import DataCard from "../components/datacard.js";
import Graph from "../components/plot.js";
import GraphTest from "../components/testplot.js";
import { MdArrowBack } from "react-icons/md";
import {
    Heading,
    Avatar,
    Button,
    Box,
    Center,
    Text,
    Stack,
    VStack,
    Link,
    Badge,
    useColorModeValue,
} from '@chakra-ui/react';
import api from "../api.js";
import { buildQueries } from "@testing-library/react";


export default function ComponentCard({ id, 
                                        name,
                                        p1,
                                        p2,
                                        img,
                                        devices,
                                        setDevices,
                                        localDevices,
                                        locations,
                                        localLocs,
                                        setLocations,
                                        user,
                                        setLocalDevices,
                                        building
                                    })
{

    console.log(locations[0].id)

    console.log(locations)

    console.log(building)


    const [devSelected, setDevSelected] = useState(false);
    const [locSelected, setLocSelected] = useState(false);

    const setCurrentDevice = () => {
        console.log(id)
        setDevices(devices.filter(dev => dev.id === id))
        setDevSelected(true)
    }

    const setCurrentLocation = () => {
        setLocations(locations.filter(loc => loc.id === id))
        setLocSelected(true)
    }

    const back = () => {
        if (devices) {

            /*  console.log(devices)
             console.log(localDevices)
             setDevSelected(false)
             setDevices(devices) */

            api.getDevices(user).then(devs => {
                setDevices(devs)
                setDevSelected(false)
            })
        }
        else {
            /* console.log(locations)
            console.log(localLocs)
            setLocSelected(false)
            setLocations(locations) */

            api.getLocations(user).then(locations => {
                setLocations(locations)
                setLocSelected(false)
            })
        }
    }

useEffect(() => {
        api
            .getLocationDevices(locations[0].id)
            .then((data) => {
                console.log(data)
                //setSensorData(data);
            })
            .catch((err) => {
                console.log(err.response.status)
            })
}, [locations])

    return (
        <>
            <Center py={6}>
                <Box
                    maxW={'225px'}
                    w={'full'}
                    bg={useColorModeValue('white', 'gray.900')}
                    boxShadow={'2xl'}
                    rounded={'lg'}
                    p={6}
                    textAlign={'center'}
                >
                    {devSelected || locSelected ?
                        <Center py={6}>
                            <Box>
                                <Button leftIcon={<MdArrowBack />} colorScheme='blue' variant='solid' onClick={back}> Takaisin </Button>
                            </Box>
                        </Center>
                        : ''
                    }

                    <Heading mb={4}> {building} </Heading>
                    <Avatar
                        size={'xl'}
                        src={img}
                        alt={'Avatar Alt'}
                        mb={4}
                        pos={'relative'}
                        onClick={() => devices ? setCurrentDevice() : setCurrentLocation()}/>
                    <Heading fontSize={'2xl'} fontFamily={'body'}>
                        {name}
                    </Heading>
                    <Text fontWeight={600} color={'gray.500'} mb={4}>
                        {/* Lämpötila  */}{p1}
                    </Text>
                    <Text
                        textAlign={'center'}
                        color={useColorModeValue('gray.700', 'gray.400')}
                        px={3}>
                        {/*  Kosteus%   */}    {p2}
                    </Text>
                </Box>
            </Center>
        
        {/* 
            {devSelected || locSelected ?
                <Center py={6}>
                    <Box pr={4}>
                       
                        <Graph />
                    </Box>
                </Center>
                : ''
            } */}


    {devSelected || locSelected ?  
        <Center py={6}>
            <Box pr={4}>
                <DataCard user={user} locations={locations}/>
            </Box>
        </Center>
         : ''      
    }  
        </>
    )
}