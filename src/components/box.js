import React, { useState, useEffect } from "react"
import api from "../api";
import Graph from "../components/plot.js"
import CSV from "../components/export_csv"
import {
    Heading,
    Avatar,
    Button,
    Box,
    Center,
    Image,
    Text,
    Stack,
    HStack,
    VStack,
    Link,
    Badge,
    useColorModeValue,
} from '@chakra-ui/react';
//import contourcarpet from "plotly.js/src/traces/contourcarpet";




export default function BoxTest({ allData,
    setData,
    lab,
    setSomething,
    labData,
    getSensData,
    graphData,
    title,
    devType
}) {


    console.log(devType)



    /*console.log(allData)
     console.log(setData)
     console.log(labData)
     console.log(graphData)
     console.log(lab)
     console.log(setSomething)
     console.log(getSensData)
     console.log(title) */

    const devTypes = ['', 'Olosuhde', 'Ympäristö', 'Elsys', 'Miles']
    const [graph, setGraph] = useState({
        title: '',
        value: '',
        time: ''
    }
    )

    const setGraphData = (title, value, time) => {
        console.log(title, value, time)
        setGraph(
            {
                title: title,
                value: value,
                time: time
            }
        )
    }

    console.log(graph)

    return (
        <>
            <Box maxW='sm' borderWidth='2px' borderRadius='lg'/*  overflow='hidden' */ my={4} ml={7} width={400} /* overflowY="scroll" */>
                <Box p='6'>
                    <Center>
                        <Box /* display='flex' alignItems='baseline' */>
                            <Badge borderRadius='full' px='2' colorScheme='teal' width="180px" height="25px" my={2}>
                                <Text align="center">
                                    {title}
                                </Text>
                            </Badge>

                        </Box>
                    </Center>

                    <Center>

                        <Box
                            mt='1'
                            fontWeight='semibold'
                            as='h4'
                            lineHeight='tight'
                            isTruncated
                            my={3}

                        >
                            {allData && allData.length ?
                                allData.map((data, index) =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => setData(data.lid)}>
                                        {/* {data.lid} */} {data.bname} - {data.lname}
                                    </Text>
                                )
                                : ''
                            }

                            {lab && lab.length ?
                                lab.map(data =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4} onClick={() => setSomething(data.lid)}>
                                        {data.lid} {data.lname}
                                    </Text>
                                )
                                : ''
                            }

                            {labData && labData.length ?
                                labData.map(data =>
                                    <Text fontWeight={600} color={'gray.500'} mb={4}

                                    /* EI EHKÄÖTARVII
                                    onClick={() => getSensData(data.id)} 
                                    */
                                    >
                                        {/* {devTypes[data.type]} */} {/* {data.id} */} {data.name}
                                    </Text>
                                )
                                : ''
                            }

                            {/*  NÄYTTÄÄ VAIN KYMMENEN  */}

                            {

                                graphData && graphData.length && devType === 1 ?
                                    graphData.slice(graphData.length - 1).map(data =>
                                        <>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Ilmankosteus', data.humidity, data.timestamp)}>
                                                {/* {data.id} {data.mac} */}
                                                Ilmankosteus {data.humidity} %
                                            </Text>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('TVoc', data.voc, data.timestamp)}>
                                                {/* {data.id} {data.mac} */}
                                                TVOC {data.voc} ppb
                                            </Text>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Hiukkaspitoisuus', data.mass_pm1, data.timestamp)}>
                                                {/* {data.id} {data.mac} */}
                                                Hiukkaspitoisuus {data.mass_pm1} ppm
                                            </Text>

                                        </>
                                    )

                                    : ''
                            }

                            {

                                graphData && graphData.length && devType === 3 ?
                                    graphData.slice(graphData.length - 1).map(data =>
                                        <>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Lämpötila', data.temp, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Lämpötila {data.temp} °C
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Kosteus', data.humidity, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Kosteus {data.humidity} %
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Hiilidioksidi', data.co2, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Hiilidioksidi {data.co2} ppm
                                            </Text>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Valoisuus', data.light, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Valoisuus {data.light} Lux
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Liike', data.motion, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Liike {data.motion}
                                            </Text>

                                            <CSV data={graphData} />  


                                            {/*     <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Ilmankosteus', data.humidity, data.timestamp)}>
                                  
                                                Liike {data.motion}
                                            </Text>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('TVoc', data.voc, data.timestamp)}>
                                              
                                                Co2 {data.co2}
                                            </Text */}
                                        </>
                                    )

                                    : ''
                            }



                            {

                                graphData && graphData.length && devType === 4 ?
                                    graphData.slice(graphData.length - 1).map(data =>
                                        <>
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Lämpötila', data.temp, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Lämpötila {data.temp} °C
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('KOSTEUS', data.hum, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Kosteus {data.hum} %
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('TVOC', data.tvoc, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                TVOC {data.tvoc} ppb
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Hiilidioksidi', data.co2, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Hiilidioksidi {data.co2} ppm
                                            </Text>

    
                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Valoisuus', data.light, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Valoisuus {data.light} Lux
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Liike', data.motion, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Liike {data.motion}
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Paine', data.pressure, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Paine {data.pressure} hPa
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('PM_2_5', data.pm_2_5, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                PM_2_5 {data.pm_2_5} μg/m3
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('PM_10', data.pm_10, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                PM_10 {data.pm_10} μg/m3
                                            </Text>

                                            <Text fontWeight={600} color={'gray.500'} mb={4} ml={4}
                                                onClick={() => setGraphData('Ozone', data.ozone, data.time)}>
                                                {/* {data.id} {data.mac} */}
                                                Formaldehydi {data.ozone} mg/m3
                                            </Text>

                                            <CSV data={graphData} />  

                                        </>
                                    )

                                    : ''
                            }






















                        </Box>
                    </Center>
                </Box>

            </Box>
            {
               /*  graphData */ graph && graph.title != '' ?
                    <Center>
                        <Box
                            mt='1'
                            /*  fontWeight='semibold' */
                            as='h4'
                            lineHeight='tight'
                            isTruncated
                            my={3}
                        >
                            <Graph graphData={graph}
                                gData={graphData}
                                devType={devType} />                 
                           
                        </Box>
                    </Center>
                    : ''
            }
    
        </>
    )
}